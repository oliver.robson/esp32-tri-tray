# ESP32 Tri-Tray

The ESP32 Tri-Tray is a 3-up breakout designed for the LilyGo TTGO MINI 32,
a D1 Mini form factor ESP32-based development board.

The purpose of this breakout is to make it easier to plug multiple shields
into the TTGO board to enable more variety in connected peripherals. While
some D1 Mini form factor boards serving this purpose already exist, they were
too narrow to account for the extra row of pins either side of the TTGO MINI
32 - this design was created to solve that by simply increasing the spacing
between boards.

The design was done in KiCad 5.
